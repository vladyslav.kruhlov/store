import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  template: `
    <div class="search">
      <input class='search__full-width' type="text" placeholder='Text to search'>
    </div>
  `,
  styles: [`
    .search {
      min-width: 500px;
      width: 100%;
      background: rgb(230,170,252);
      background: linear-gradient(90deg, rgba(230,170,252,1) 0%, rgba(165,226,255,1) 100%);
      padding: 2px 15px 2px 2px;
      border-radius: 7px;

      &__full-width {
        width: 100%;
        height: 45px;
        font-size: 14px;
        border: none;
        border-radius: 5px;
        color: black;
        padding-left: 10px;
        padding-right: 2px;

        &:focus {
          outline: none;
        }
      }
    }
  `]
})
export class InputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

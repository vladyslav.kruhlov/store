import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  template: `
    <button class='button' mat-raised-button>Search</button>
  `,
  styles: [`
    .button {
      width: 100px;
      height: 48px;
      background: rgb(230,170,252);
      background: linear-gradient(90deg, rgba(230,170,252,1) 0%, rgba(165,226,255,1) 100%);
    }
  `]
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

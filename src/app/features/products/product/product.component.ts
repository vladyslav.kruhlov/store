import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent implements OnInit {
  products = [
    { title: 'Iphone 13 Pro Max', price: 1200 },
    { title: 'Iphone 13 Pro Max', price: 1200 },
    { title: 'Iphone 13 Pro Max', price: 1200 },
    { title: 'Iphone 13 Pro Max', price: 1200 },
    { title: 'Iphone 13 Pro Max', price: 1200 },
    { title: 'Iphone 13 Pro Max', price: 1200 },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
